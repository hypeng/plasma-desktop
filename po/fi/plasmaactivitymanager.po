# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-06-26 00:19+0000\n"
"PO-Revision-Date: 2016-05-01 00:30+0200\n"
"Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: sortedactivitiesmodel.cpp:316
#, kde-format
msgid "Used some time ago"
msgstr "Käytetty jokin aika sitten"

#: sortedactivitiesmodel.cpp:332
#, kde-format
msgid "Used more than a year ago"
msgstr "Käytetty yli vuosi sitten"

#: sortedactivitiesmodel.cpp:333
#, kde-format
msgctxt "amount in months"
msgid "Used a month ago"
msgid_plural "Used %1 months ago"
msgstr[0] "Käytetty kuukausi sitten"
msgstr[1] "Käytetty %1 kuukautta sitten"

#: sortedactivitiesmodel.cpp:334
#, kde-format
msgctxt "amount in days"
msgid "Used a day ago"
msgid_plural "Used %1 days ago"
msgstr[0] "Käytetty päivä sitten"
msgstr[1] "Käytetty %1 päivää sitten"

#: sortedactivitiesmodel.cpp:335
#, kde-format
msgctxt "amount in hours"
msgid "Used an hour ago"
msgid_plural "Used %1 hours ago"
msgstr[0] "Käytetty tunti sitten"
msgstr[1] "Käytetty %1 tuntia sitten"

#: sortedactivitiesmodel.cpp:336
#, kde-format
msgctxt "amount in minutes"
msgid "Used a minute ago"
msgid_plural "Used %1 minutes ago"
msgstr[0] "Käytetty minuutti sitten"
msgstr[1] "Käytetty %1 minuuttia sitten"

#: sortedactivitiesmodel.cpp:337
#, kde-format
msgid "Used a moment ago"
msgstr "Käytetty hetki sitten"

#: switcherbackend.cpp:166
#, kde-format
msgid "Walk through activities"
msgstr "Selaa aktiviteetteja"

#: switcherbackend.cpp:171
#, kde-format
msgid "Walk through activities (Reverse)"
msgstr "Selaa aktiviteetteja (käänteisessä järjestyksessä)"

#~ msgid "Used a long time ago"
#~ msgstr "Käytetty kauan sitten"
