# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2010, 2011, 2012, 2013, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-16 00:49+0000\n"
"PO-Revision-Date: 2021-09-23 12:20+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: contents/ui/main.qml:103
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Aperi"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Vacue"

#: contents/ui/main.qml:110
#, kde-format
msgid "Trash Settings…"
msgstr "Preferentias de corbe..."

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Corbe\n"
"Vacue"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Corbe\n"
"Un elemento"
msgstr[1] ""
"Corbe\n"
"%1 elementos"

#: contents/ui/main.qml:170
#, kde-format
msgid "Trash"
msgstr "Corbe"

#: contents/ui/main.qml:171
#, kde-format
msgid "Empty"
msgstr "Vacue"

#: contents/ui/main.qml:171
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Un elemento"
msgstr[1] "%1 elementos"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "Corbe\n"
#~ " Vacue"

#~ msgid "Empty Trash"
#~ msgstr "Vacua corbe"

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "Tu vermente vole vacuar le corbe? Omne elementos essera delite."

#~ msgid "Cancel"
#~ msgstr "Cancella"

#~ msgid "&Empty Trashcan"
#~ msgstr "&Vacua Corbe"

#~ msgid "&Menu"
#~ msgstr "&Menu"

#~ msgctxt "@title:window"
#~ msgid "Empty Trash"
#~ msgstr "Vacua corbe"

#~ msgid "Emptying Trashcan..."
#~ msgstr "Il es vacuante le corbe..."
