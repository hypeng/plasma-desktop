# Translation of plasma_applet_org.kde.plasma.kickoff to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-31 00:47+0000\n"
"PO-Revision-Date: 2022-10-30 09:45+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Generelt"

#: package/contents/ui/code/tools.js:49
#, kde-format
msgid "Remove from Favorites"
msgstr "Fjern frå favorittar"

#: package/contents/ui/code/tools.js:53
#, kde-format
msgid "Add to Favorites"
msgstr "Legg til favorittar"

#: package/contents/ui/code/tools.js:77
#, kde-format
msgid "On All Activities"
msgstr "På alle aktivitetar"

#: package/contents/ui/code/tools.js:127
#, kde-format
msgid "On the Current Activity"
msgstr "På gjeldande aktivitet"

#: package/contents/ui/code/tools.js:141
#, kde-format
msgid "Show in Favorites"
msgstr "Vis i favorittar"

#: package/contents/ui/ConfigGeneral.qml:37
#, kde-format
msgid "Icon:"
msgstr "Ikon:"

#: package/contents/ui/ConfigGeneral.qml:72
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Vel …"

#: package/contents/ui/ConfigGeneral.qml:77
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Tilbakestill til standardikon"

#: package/contents/ui/ConfigGeneral.qml:83
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr "Fjern ikon"

#: package/contents/ui/ConfigGeneral.qml:94
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "Tekst-merkelapp:"

#: package/contents/ui/ConfigGeneral.qml:96
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "Skriv her for å leggja til tekst-merkelapp"

#: package/contents/ui/ConfigGeneral.qml:124
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr "Du kan ikkje laga ein tekst-merkelapp når panelet står loddrett."

#: package/contents/ui/ConfigGeneral.qml:135
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "Generelt:"

#: package/contents/ui/ConfigGeneral.qml:136
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Sorter alltid programma alfabetisk"

#: package/contents/ui/ConfigGeneral.qml:141
#, kde-format
msgid "Use compact list item style"
msgstr "Bruk kompakt listestil"

#: package/contents/ui/ConfigGeneral.qml:147
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr "Vert automatisk slått av i trykksjerm-modus"

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Set opp påslegne søkjetillegg …"

#: package/contents/ui/ConfigGeneral.qml:166
#, kde-format
msgid "Show favorites:"
msgstr "Vis favorittar:"

#: package/contents/ui/ConfigGeneral.qml:167
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "På rutenett"

#: package/contents/ui/ConfigGeneral.qml:175
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "Som liste"

#: package/contents/ui/ConfigGeneral.qml:183
#, kde-format
msgid "Show other applications:"
msgstr "Vis andre program:"

#: package/contents/ui/ConfigGeneral.qml:184
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "På rutenett"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "Som liste"

#: package/contents/ui/ConfigGeneral.qml:204
#, kde-format
msgid "Show buttons for:"
msgstr "Vis knappar for:"

#: package/contents/ui/ConfigGeneral.qml:205
#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "Power"
msgstr "Straum"

#: package/contents/ui/ConfigGeneral.qml:214
#, kde-format
msgid "Session"
msgstr "Økt"

#: package/contents/ui/ConfigGeneral.qml:223
#, kde-format
msgid "Power and session"
msgstr "Straum og økt"

#: package/contents/ui/ConfigGeneral.qml:232
#, kde-format
msgid "Show action button captions"
msgstr "Vis tekst på handlingsknappar"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Program"

#: package/contents/ui/Footer.qml:110
#, kde-format
msgid "Places"
msgstr "Stadar"

#: package/contents/ui/FullRepresentation.qml:119
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr "Ingen treff"

#: package/contents/ui/Header.qml:64
#, kde-format
msgid "Open user settings"
msgstr "Opna brukarinnstillingar"

#: package/contents/ui/Header.qml:240
#, kde-format
msgid "Keep Open"
msgstr "Hald open"

#: package/contents/ui/Kickoff.qml:298
#, kde-format
msgid "Edit Applications…"
msgstr "Rediger program …"

#: package/contents/ui/KickoffGridView.qml:88
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "Rutenett med %1 rader og %2 kolonnar"

#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "Leave"
msgstr "Avslutt"

#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "More"
msgstr "Meir"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Data­maskin"

#: package/contents/ui/PlacesPage.qml:51
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Logg"

#: package/contents/ui/PlacesPage.qml:52
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Mest brukte"
