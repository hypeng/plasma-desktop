msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2022-08-26 02:36+0100\n"
"Last-Translator: Pedro Morais <morais@kde.org>\n"
"Language-Team: pt <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Spell-Extra: web ms tab trackball clicks click\n"
"X-Spell-Extra: esquerdino multi\n"
"X-POFile-SpellExtra: FX Live RF can trackball TrackMan MX MouseMan Hughes\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Dowler Faure Gehrmann Mueller Logitech Hemsley Nolden\n"
"X-POFile-SpellExtra: libusb Rik ms Brad Dirk Ralf Hards Bernd KD Höglund\n"
"X-POFile-SpellExtra: Fredrik KCM Roman Gilg tácteis\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Pedro Morais,José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "morais@kde.org,zepires@gmail.com"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"A pesquisa pelos dispositivos de entrada foi mal-sucedida. Por favor, reabra "
"este módulo de configuração."

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr "Erro crítico ao ler os dados fundamentais do dispositivo %1."

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr "KCM do dispositivo de ponteiro"

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr ""
"Módulo da Configuração do Sistema para gerir ratos e ponteiros com bola."

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr "Copyright 2018 Roman Gilg"

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr "Roman Gilg"

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr "Desenvolvimento"

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"Ocorreu um erro ao carregar os valores. Veja os registos para obter mais "
"informações. Por favor, reinicie este módulo de configuração."

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr "Não foi encontrado nenhum dispositivo. Ligue-o agora."

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Não foi possível gravar todas as modificações. Veja os registos para obter "
"mais informações. Por favor, reinicie este módulo de configuração e tente de "
"novo."

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"Ocorreu um erro ao ler os valores predefinidos. Nem todas as opções puderam "
"ser configuradas com os seus valores por omissão."

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"Ocorreu um erro ao adicionar o dispositivo acabado de ligar. Por favor, "
"ligue-o de novo e reinicie este módulo de configuração."

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""
"O dispositivo de ponteiro desligou-se. Foi fechada a sua janela de "
"configuração."

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""
"O dispositivo de ponteiro desligou-se. Não foram encontrados mais "
"dispositivos."

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr "Dispositivo:"

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, kde-format
msgid "General:"
msgstr "Geral:"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr "Dispositivo activo"

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr "Aceitar a introdução de dados por este dispositivo."

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, kde-format
msgid "Left handed mode"
msgstr "Modo esquerdino"

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr "Trocar os botões esquerdo e direito."

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""
"Carregue nos botões esquerdo e direito para um 'click' do botão do meio"

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"Se carregar nos botões esquerdo e direito em simultâneo, envia um 'click' do "
"botão do meio."

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, kde-format
msgid "Pointer speed:"
msgstr "Velocidade do ponteiro:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, kde-format
msgid "Acceleration profile:"
msgstr "Perfil de aceleração:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr "Plano"

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr "O cursor move a mesma distância que o movimento do rato."

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr "Adaptativo"

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""
"A distância de percurso do cursor depende da velocidade do movimento do dedo."

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr "Deslocamento:"

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, kde-format
msgid "Invert scroll direction"
msgstr "Inverter a direcção do deslocamento"

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Deslocamento como nos ecrãs tácteis."

#: kcm/libinput/main.qml:295
#, kde-format
msgid "Scrolling speed:"
msgstr "Velocidade do deslocamento:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Mais devagar"

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Mais depressa"

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr "Voltar a Associar os Botões Adicionais do Rato..."

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr "Botão Extra %1:"

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""
"Carregue no botão do rato ao qual deseja associar uma combinação de teclas"

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr "Indique a nova combinação de teclas para o %1"

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancelar"

#: kcm/libinput/main.qml:448
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Carregue num botão do rato."

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr "Nova Associação…"

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr "Recuar"

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""
"<h1>Rato</h1> Este módulo permite ao utilizador escolher várias operações "
"sobre a maneira como o seu dispositivo de ponteiro funciona. O dispositivo "
"poderá ser um rato, uma 'trackball' ou outro 'hardware' que desempenhe uma "
"função semelhante."

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&Geral"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""
"Se o utilizador for esquerdino, poderá preferir trocar as funções dos botões "
"esquerdo e direito do seu dispositivo escolhendo a opção 'esquerdino'. Se o "
"seu dispositivo tiver mais do que dois botões, só os que funcionam como "
"esquerdo e direito é que são afectados. Por exemplo, se tiver um rato de "
"três botões, o botão do meio não é afectado."

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "Ordem dos Botões"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "Des&tro"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "Es&querdino"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""
"Muda a direcção do deslocamento para a roda ou para os 4º e 5º botões do "
"rato."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "In&verter a direcção do deslizamento"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Avançado"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "Aceleração do cursor:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "Sensibilidade do dispositivo:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "Intervalo de duplo-click:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "Tempo de início de arrasto:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "Distância até arrastar:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "A roda do rato desloca:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""
"<p>Esta opção permite-lhe alterar a relação entre a distância que o cursor "
"percorre no ecrã e o movimento relativo do dispositivo físico em si (que "
"pode ser um rato, 'trackball' ou outro dispositivo do género).</p><p> Um "
"valor elevado para a aceleração levará a grandes movimentos do cursor no "
"ecrã mesmo que o utilizador só faça um pequeno movimento com o dispositivo "
"físico. A selecção de valores muito elevados poderá resultar em que o cursor "
"voe pelo ecrã, tornando-o difícil de controlar.</p>"

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr " x"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""
"<p>A sensibilidade é a menor distância que o cursor do rato deverá "
"movimentar-se no ecrã antes que a aceleração tenha algum efeito. Se o "
"movimento for menor que a sensibilidade, o cursor do rato mover-se-á como se "
"a aceleração fosse 1x.</p><p> Desta maneira, quando o utilizador fizer "
"movimentos pequenos com o dispositivo físico, não existirá aceleração "
"nenhuma, dando-lhe um maior grau de controlo sobre o cursor do rato. Com "
"movimentos mais amplos do dispositivo, poderá então mover o cursor mais "
"rapidamente para diferentes áreas do ecrã.</p>"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""
"O intervalo de duplo-'click' é o tempo máximo (em milisegundos) entre dois "
"'clicks' de rato para serem considerados como um duplo 'click'. Se o segundo "
"'click' ocorrer depois desse intervalo a seguir ao primeiro, são "
"reconhecidos com dois 'clicks' separados."

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " ms"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"Se o utilizador carregar com o rato (p. ex. num editor multi-linha) e "
"começar a mover o rato dentro do tempo de início de arrasto, será iniciada "
"uma operação de arrasto."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"Se o utilizador carregar com o rato e o mover pelo menos esta distância, "
"será iniciada uma operação de arrasto."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""
"Se o utilizador usar a roda do rato, este valor determina o número de linhas "
"que são deslocadas por cada movimento da roda. Lembre-se que se este valor "
"for maior que o número de linhas visíveis, será ignorado e o movimento da "
"roda será tratado como um movimento de página acima/abaixo."

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, kde-format
msgid "Keyboard Navigation"
msgstr "Navegação com o Teclado"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "&Mover o cursor com o teclado (numérico)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "&Atraso de aceleração:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "Intervalo de r&epetição:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "&Tempo de aceleração:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "Velocidade má&xima:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr " pontos/s"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "&Perfil de aceleração:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr "Rato"

#: kcm/xlib/xlib_config.cpp:83
#, kde-format
msgid "(c) 1997 - 2018 Mouse developers"
msgstr "(c) 1997 - 2018 equipa de desenvolvimento do Rato"

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr "Patrick Dowler"

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr "Dirk A. Mueller"

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr "Bernd Gehrmann"

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr "Rik Hemsley"

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr "Brad Hughes"

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr "Brad Hards"

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " ponto"
msgstr[1] " pontos"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] " linha"
msgstr[1] " linhas"
